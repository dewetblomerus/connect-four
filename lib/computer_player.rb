class ComputerPlayer
  attr_reader :color, :strength
  def initialize(color:, strength: 2)
    @color = color
    @opponent_color = ["Yellow", "Red"].find { |c| c != @color }
    @strength = strength
  end

  def decide_move(game:)
    moves = game.legal_moves
    moves = block_opponent_win(game) if @strength == 2
    if @strength > 2

      minimax(game, 0)
      moves = [@best_choice]
    end
    moves.sample
  end

  private

  # I this method is the only piece that is not my original code
  # I got it from here and made some changes http://neverstopbuilding.com/minimax
  def minimax(game, depth)
    return score(game) if game.over?
    return 0 if depth > @strength + 1
    scores = [ ]
    moves = [ ]

    game.legal_moves.each do |move|
      possible_game = new_game_state(game, move)
      scores.push minimax(possible_game, depth + 1)
      moves.push move
    end

    if game.current_player == @color[0]
      # This is the max calculation
      max_score_index = scores.each_with_index.max[1]
      @choice = moves[max_score_index]
      @best_choice = @choice
      return scores[max_score_index]
    else
      # This is the min calculation
      min_score_index = scores.each_with_index.min[1]
      @choice = moves[min_score_index]
      return scores[min_score_index]
    end
  end

  def new_game_state(game, move=false)
    temp_game = Marshal.load( Marshal.dump(game) )
    temp_game.move(move) if move
    temp_game
  end

  def score(game)
    if game.status == @color
      10
    elsif game.status == @opponent_color
      -10
    else
      0
    end
  end

  def block_opponent_win(game)
    game.legal_moves.each do |move|
      temp_game = Marshal.load( Marshal.dump(game) )
      temp_game.board.move(color: @opponent_color, move_column: move)
      if temp_game.over?
	return [move]
      end
    end
    game.legal_moves
  end
end
