module BoardPrinter
  def self.print_board(board)
    matrix_string = "  0   1   2   3   4   5   6\n"
    board.each do |row|
      row_string = String.new
      row.each do |circle|
	row_string << " #{circle} |"
      end
      matrix_string << "|#{row_string}\n"
    end
    matrix_string
  end
end
