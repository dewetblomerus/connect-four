class HumanCli
  attr_reader :color
  def initialize(color)
    @color = color.capitalize
  end

  def decide_move(game: '_')
    puts "Choose a column to drop your checker into"
    move_column = gets.chomp!
    move_column
  end

end
