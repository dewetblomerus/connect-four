class Board
  attr_accessor :matrix
  def initialize(matrix: false)
    @matrix = matrix || build_matrix
  end

  def build_matrix
    new_matrix = Array.new

    6.times do
      new_matrix << Array.new(7, " ")
    end
    new_matrix
  end

  def legal_moves
    @matrix[0].each_index.select { |i| @matrix[0][i] == " " }
  end

  def move(color:, move_column:)
    move_column = move_column.to_i
    @matrix.reverse.each do |row|
      if row[move_column] == " "
	row[move_column] = color[0]
	break
      end
    end
  end
end

