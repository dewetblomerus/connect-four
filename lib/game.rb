class Game
  attr_reader :board, :player1, :player2, :players
  attr_accessor :current_player

  def initialize(board: false, player1: "Y", player2: "R")
    @board = board || Board.new
    @player1 = player1
    @player2 = player2
    @players = [@player1, @player2]
    @status = "New"
    @current_player = player1
  end

  def switch_sides
    @current_player = @players.find { |p| p != @current_player }
  end

  def move(column)
    @board.move(color: @current_player, move_column: column)
    switch_sides
  end

  def status
    check_status
    @status
  end

  def over?
    check_status
    return false if @status == "New"
    true
  end

  def legal_moves
    @board.legal_moves
  end

  private

  def check_status
    check_horizontal_win(@board.matrix)
    check_vertical_win(@board.matrix)
    check_diagonal_desc
    check_draw
  end

  def check_diagonal_desc
    diagonal_desc_rows = yaw(@board.matrix)[:right]
    check_vertical_win(diagonal_desc_rows)
    diagonal_asc_rows = yaw(@board.matrix)[:left]
    check_vertical_win(diagonal_asc_rows)
  end

  def yaw(matrix)
    yawed = {right: [], left: []}
    size = matrix.size - 1
    matrix.each_with_index do |old_row, i|
      yaw_right_row = Array.new((size - i), " ")
      yaw_right_row.concat old_row
      yaw_right_row.concat Array.new(i, " ")
      yawed[:right] << yaw_right_row
      yaw_left_row = Array.new(i, " ")
      yaw_left_row.concat old_row
      yaw_left_row.concat Array.new((size - i), " ")
      yawed[:left] << yaw_left_row
    end
    yawed
  end

  def check_horizontal_win(matrix)
    matrix.each do |row|
      break if check_row_for_win(row)
    end
  end

  def check_vertical_win(matrix)
    (matrix.transpose.map &:reverse).each do |row|
      break if check_row_for_win(row)
    end
  end

  def check_row_for_win(row)
    if row.join =~ /R{4}|Y{4}/
      winner = row.join.scan(/R{4}|Y{4}/)[0][0]
      @status = "#{expand_player_color(winner)}"
      true
    end
  end

  def check_draw
    unless @board.matrix.flatten.include? " "
      @status = "Draw"
    end
  end

  def expand_player_color(letter)
    if letter == "R"
      "Red"
    elsif letter == "Y"
      "Yellow"
    else
      raise "Unexpected player color, expecting Y or R"
    end
  end
end
