FROM ruby:2.4.1
RUN mkdir /root/connect-four
WORKDIR /root/connect-four
ADD . /root/connect-four/
RUN gem install bundler
RUN bundle -j3
CMD /root/connect-four/play.rb

