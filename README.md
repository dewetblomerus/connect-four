# Play Connect Four from the comfort of your terminal

### If you have Docker installed, just copy/paste this command and the game will run.

```
docker run -it registry.gitlab.com/dewetblomerus/connect-four
```

### Install locally.

```
bundle install

./play.rb

```

### Run the tests

```
bundle exec rspec
```
