#!/usr/bin/env ruby
require 'pry'
require 'require_all'
require_all 'lib'

puts "\n\n\n"
puts "#######################"
puts "Welcome to Connect Four"
puts "#######################"

player1 = HumanCli.new("Yellow")

def gets_integer(min:, max:)
  input = -9999999
  until input.between?(min, max)
    puts "Enter a number netween #{min} and #{max}"
    input = gets.chomp.to_i
  end
  input
end

puts "\nSelect your game type:
1. Single player (Against Computer)
2. Two player"
game_type = gets_integer(min: 1, max: 2)

if game_type == 2
  player2 = HumanCli.new("Red")
elsif game_type == 1
  puts "How strong should the computer play?
  1. Random.
  2. Blocks when it can.
  3. Looks ahead intelligently.
  4. Looks ahead even further, on this setting the computer takes a long time to move."
  ai_strength = gets_integer(min: 1, max: 4)
  player2 = ComputerPlayer.new(color: "Red", strength: ai_strength)
else
  puts "How on earth did you choose that?"
end

game = Game.new

until game.over?
  [player1, player2].each do |player|
    puts "\n\n\n"
    break if game.over?
    puts BoardPrinter.print_board(game.board.matrix)
    puts "#{player.color}'s turn."
    move_column = player.decide_move(game: game)
    game.move(move_column)
  end
end

puts BoardPrinter.print_board(game.board.matrix)

if game.status == "Draw"
  puts "Bummer, it's a draw. Have fun playing again."
elsif game.status == "Yellow" || (game.status == "Red" && game_type == 2)
  puts "Way to go #{game.status}! You won"
elsif game.status == "Red"
  puts "Sorry Yellow, the computer beat you this time, try again."
else
  puts "Something has gone horribly wrong"
end

