require_relative '../lib/board_printer'
require_relative '../lib/board'

RSpec.describe BoardPrinter do
  it "prints an empty board such that the rows and columns are clear" do
    expect(BoardPrinter.print_board(Board.new.matrix)).to eq(
"  0   1   2   3   4   5   6
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
"
    )
  end

end
