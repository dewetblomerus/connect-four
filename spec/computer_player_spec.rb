require_relative '../lib/computer_player'
require_relative 'example_boards'
require 'pry'

RSpec.describe ComputerPlayer do
  let(:computer) { ComputerPlayer.new(color: "Yellow") }
  let(:computer_red) { ComputerPlayer.new(color: "Red") }

  it 'blocks an opponent from vertical 4' do
    expect(computer.decide_move(game: build_game(RED_3_VERTICAL_AT_1))).to eq(1)
    expect(computer.decide_move(game: build_game(RED_3_VERTICAL_AT_4))).to eq(4)
  end

  it 'blocks an opponent from horizontal 4' do
    expect(computer.decide_move(game: build_game(RED_3_HORIZONTAL_CONTINUOUS))).to eq(5)
    expect(computer_red.decide_move(game: build_game(YELLOW_3_HORIZONTAL_GAP))).to eq(3)
  end

  # Skip this because random is more fun
  xit 'can be set to lower strength' do
    expect(computer.strength).to eq(2)
    weak = ComputerPlayer.new(color: "Red", strength:	1)
    expect(weak.decide_move(game: build_game(YELLOW_3_HORIZONTAL_GAP))).to eq(0)
  end

  it 'can be set to super hard' do
    insane = ComputerPlayer.new(color: "Red", strength:	3)
    expect(insane.decide_move(game: build_game(YELLOW_3_HORIZONTAL_GAP))).to eq(3)
    expect(insane.decide_move(game: build_game(RED_CAN_WIN_2))).to eq(2)
  end
end

