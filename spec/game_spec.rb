require_relative '../lib/game'
require_relative 'example_boards'

RSpec.describe Game do
  let(:player1) { "Yellow" }
  let(:player2) { "Red" }
  let(:new_game) { Game.new(board: Board.new, player1: "Yellow", player2: "Red") }

  it "makes two players available" do
    expect(new_game.players[0]).to eq("Yellow")
  end

  it "detects when someone won horizontally" do
    board = Board.new
    board.matrix = RED_WINS_HORIZONTAL
    game = Game.new(board: board, player1: player1, player2: player2)
    expect(game.over?).to eq(true)
    expect(game.status).to eq("Red")

    board2 = Board.new
    board2.matrix = YELLOW_WINS_HORIZONTAL
    game2 = Game.new(board: board2, player1: player1, player2: player2)
    expect(game2.over?).to eq(true)
    expect(game2.status).to eq("Yellow")
  end

  it "detects when the game is not over" do
    board = Board.new
    game = Game.new(board: board, player1: player1, player2: player2)
    expect(game.over?).to eq(false)
    expect(game.status).to eq("New")
  end

  it "detects when someone won vertically" do
    board = Board.new
    board.matrix = RED_WINS_VERTICAL
    game = Game.new(board: board, player1: player1, player2: player2)
    expect(game.over?).to eq(true)
    expect(game.status).to eq("Red")
  end

  it "detects when someone won diagonally descending" do
    board = Board.new
    board.matrix = RED_WINS_DIAGONAL_DESC
    game = Game.new(board: board, player1: player1, player2: player2)
    expect(game.over?).to eq(true)
    expect(game.status).to eq("Red")
  end

  it "detects when someone won diagonally ascending" do
    board = Board.new
    board.matrix = YELLOW_WINS_DIAGONAL_ASC
    game = Game.new(board: board, player1: player1, player2: player2)
    expect(game.over?).to eq(true)
    expect(game.status).to eq("Yellow")
  end

  it "detects a draw" do
    board = Board.new
    board.matrix = DRAW_BOARD
    game = Game.new(board: board, player1: player1, player2: player2)
    expect(game.over?).to eq(true)
    expect(game.status).to eq("Draw")
  end

  it "lets players take alternating turns" do
    game = Game.new(board: Board.new, player1: player1, player2: player2)
    expect(game.current_player).to eq(player1)
    game.move(0)
    expect(game.board.matrix[-1][0]).to eq("Y")
    game.move(0)
    expect(game.board.matrix[-2][0]).to eq("R")
    game.move(0)
    expect(game.board.matrix[-3][0]).to eq("Y")
  end
end
