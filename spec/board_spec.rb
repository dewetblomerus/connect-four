require_relative '../lib/board'
require 'pry'

RSpec.describe Board do
  let(:board) { Board.new }

  it "defaults to 7 x 6" do
    expect(board.matrix.size).to eq(6)
    expect(board.matrix[0].size).to eq(7)
  end

  it "knows available legal moves" do
    expect(board.legal_moves).to eq([0, 1, 2, 3, 4, 5, 6])
    board_5_legal = Board.new(matrix: ONLY_5_LEGAL_MOVE)
    expect(board_5_legal.legal_moves).to eq([5])
    board_1_legal = Board.new(matrix: ONLY_1_LEGAL_MOVE)
    expect(board_1_legal.legal_moves).to eq([1])
  end

  it "starts off with all empty slots" do
    expect(board.matrix[0][0]).to eq(" ")
  end

  it "takes a players move and drops it to the bottom" do
    board = Board.new
    board.move(color: "Yellow", move_column: "2")
    expect(BoardPrinter.print_board(board.matrix)).to eq(
"  0   1   2   3   4   5   6
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   | Y |   |   |   |   |
"
    )

    board.move(color: "Red", move_column: "2")
    expect(BoardPrinter.print_board(board.matrix)).to eq(
"  0   1   2   3   4   5   6
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   | R |   |   |   |   |
|   |   | Y |   |   |   |   |
"
    )

  end
end
